FILES:

Samples:
2901_A. WT, Luciferase KD control
2901_D. WT, PCNA,FANCD2,FANCI KD
2901_I. WT virus stock
2901_J. ∆UL138STOP virus stock

HCMV Genome from NCBI:
https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?lvl=0&id=10359
HCMV_Genome.fasta

Local alignment-- 2901_D against HCMV_Genome:
Local_align_results.txt






CODES:

R_code_align_extract.R

Reads the fasta file and makes alignments to determine matching and unmatching sequences.

Biostrings: This is a package in R that helps you perform these alignment functions. 
msa: This package includes multiple sequence alignment algorithms ClustalW, ClustalOmega, and Muscle. These alignment algoithms contain the same parameters: cluster, gapOpening, gapExtension, maxiters, substitutionMatrix, order, type, andverbose. You can input your reference and query sequences and print out the final output alignments to determine which sequences are highly matched, and which ones have low matching quality.



Find_Junctions.py

Reads your filtered and assembled reference and query fasta genome files, parses through each, and determines novel junction sites (substitutions, deletions, insertions). Global alignment is used in this case. Biopython must be installed into system.

Seq.IO: a tool associsted with Biopython that can read and parse thorugh sequence files such as fasta files.
pairwise2: a tool associated with Biopython that can achieve both global and local alignments by taking your parsed reference and query strings, and aligning both. After making global alignments, the rest of the code iterates through each base pairing and prints out the output of positions of either a substitution, deletion, or insertion event.



R_Code_Gene_Mapping.py

Builds a circular map of genome junction sites.

circlize: This is an R package that helps build your circular genome maps. Your first set your sector and track values, and then set the size of your map to encompass the size of your genome of interest. Then you manually input links to the junction sites based on your results.