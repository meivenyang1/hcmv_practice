from Bio import SeqIO
from Bio.Seq import Seq
from Bio import pairwise2

def find_junctions(ref, que):
    # read in reference sequence from fasta file
    ref_seq = str(SeqIO.parse(ref, "fasta"))
    # read in query sequence from fasta file
    query_seq = str(SeqIO.parse(que, "fasta"))
    # Perform pairwise sequence alignment
    alignments = pairwise2.align.globalxx(ref_seq, query_seq)
    # Choose the best alignment based on the alignment score
    best_alignment = max(alignments, key=lambda x: x.score)
    # Extract the aligned sequences from the best alignment
    aligned_ref_seq, aligned_query_seq = best_alignment[:2]
    # Initialize position counter
    pos = 0
    # Iterate through the aligned sequences and compare each position
    for i in range(len(aligned_ref_seq)):
        ref_base = aligned_ref_seq[i]
        query_base = aligned_query_seq[i]
        # Check for gaps in the reference sequence
        if ref_base == "-":
            print(f"Gap at position {pos}")
        else:
            # Identify substitutions
            if ref_base != query_base:
                print(f"Substitution at position {pos}: {ref_base} -> {query_base}")
            # Identify deletions
            if query_base == "-":
                print(f"Deletion at position {pos}: {ref_base} deleted")
            # Identify insertions
            if ref_base == "-":
                print(f"Insertion at position {pos}: {query_base} inserted")
        # Increment position counter
        pos += 1



def main():
    '''
    Makes a global alignment between your reference and query sequence, and
    finds novel junctions in your query sequence (insertions, substitutions, and deletions).
    '''
    ref = "2901_I_S89_R2_001.fasta"
    que = "2901_D5_S68_R2_001.fasta"
    find_junctions(ref, que)
if __name__ == '__main__':
    main()