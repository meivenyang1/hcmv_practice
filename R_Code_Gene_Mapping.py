install.packages("circlize")
library(circlize)

tiff("rplot.tif", width = 4, height = 5, units = 'in', res = 300) #save as high resolution
circos.par("track.height" = 0.1, "start.degree" = 90)
circos.initialize(factors = "genome", xlim = c(0, 1))
sectors = "a"
s1 = factor(sectors)
circos.initialize(s1, xlim = c(0, 236))
circos.track(ylim = c(0, 1))
circos.axis(major.at = c(0, 20, 40, 60, 80, 100, 120, 140, 160, 180, 200, 220, 236),
    labels = c(0, 20, 40, 60, 80, 100, 120, 140, 160, 180, 200, 220, 236), minor.ticks = 9, 
    major.tick.length = mm_y(1), labels.cex = 0.5, labels.facing = "clockwise")
circos.link("a", 65, "a", 65, h.ratio = 0.5, lwd = 0.5)
circos.link("a", 98, "a", 99, h.ratio = 0.5, lwd = 0.5)
text(0,0, "PCNA")
circos.clear()
dev.off() #must include to save as high resolution